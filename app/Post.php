<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{

    public $primaryKey = 'id';
    protected $fillable = ['title', 'preview','body'];

    protected $table = 'posts';
    public $timestamps = true;

    public function user() {
    	return $this->belongsTo('App\User');
    }
}
