<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
    	return view('inandqe.index', compact('title'));
    }

    public function questions(){
        return view('inandqe.question', compact('title'));

    }

}
