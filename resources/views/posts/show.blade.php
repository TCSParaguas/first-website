@extends('layouts.app')

@section('content')

	<div class="buttononpostsedit">
		<a href="/posts" class="btn btn-secondary"> Previous page </a>
	</div>

	<br>

	<center>

	<button type="button" class="btn btn-success" onclick="firstFunction()">Increase font size (Refresh to undo)</button>

	</center>

	<br>
	<div class="borderaroundtitle">
		<h1> <center>{{$post->title}}</center> </h1>
	</div>
	<div class="borderaroundsomething">
		<div class="articleprelinestatus" id="increasefontsize">
			{{$post->body}}
		</div>
	</div>

	<hr>

	<div id="showbladepostsCRUDbuttons">

		<a href="/posts/{{$post->id}}/edit" class="btn btn-primary"> Edit </a>

		<br>
		<br>

		<form id="showbladepostsCRUDbuttons" display='inline-block' method="post" action="{{action('PostsController@destroy', $post->id) }}">
		    @method('delete')
		    @csrf
	        <div class="form-group">
				<input type="submit" value="Delete" class="btn btn-danger">
			</div>
		</form>

	</div>
 
	<script>
	function firstFunction() {
	  document.getElementById("increasefontsize").style.fontSize = "200%";
	}
	</script>




@endsection