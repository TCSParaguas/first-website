@extends('layouts.app')

@section('content')

	<h1 class="roundborder"> <center> Journal Articles <center> </h1>

  <br>

  <div class="buttononquestionspage"> 
      <a href="/posts/create" class="btn main-btn pull-center btn-lg btn-success btn-block" style="color: white; font-weight: bold">
        Publish
      </a>
      <br>
  </div>

  <div class="indexcard">
    <div class="container-fluid">
      <div class="row">

      @foreach($posts as $post)

        <div class="col-sm-4 py-2">
          <div class="card">
            <div class="card-body">
              <div class="card-body text-center">
              <h5 class="card-title" style="color: white">{{ $post->title }}</h5>
              <p class="card-text" style="color: white">{{ $post->preview }}</p>
              <div class="buttonsonarticlesedit">
                <a href="{{action('PostsController@show', $post->id) }}" class="btn btn-success btn-block" style="border-radius:4px;">Read</a> 
              </div>
              <br>
              <small style="color: white;">Published on {{$post->created_at}}</small>
              </div> 
            </div>
          </div>
        </div>

      @endforeach

      </div>
    </div>
  </div>

	<!-- 
	@if(count($posts) > 1)
		@foreach($posts as $post)
			<div class="well">
				<h3>{{$post->title}}</h3>
				<small>Written on {{$post->created_at}}</small>
			</div>
		@endforeach
	@else
		<p> No posts found</p>
	@endif

	-->
@endsection