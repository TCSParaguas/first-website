 @extends('layouts.app')

@section('content')

	<br>
	<h1> <center> Edit Article </center> </h1>

  <div class="container">

    <form method="post" action="{{action('PostsController@update', $post->id) }}">
      {{csrf_field()}}
      @method('PUT')
      <div class="form-group">
        <textarea type="text" name="title" placeholder="Enter your title" class="form-control">{{ $post->title }}</textarea>
      </div>
      <div class="form-group">
        <textarea type="text" style="height: 100px;" name="preview" placeholder="Preview of article" class="form-control">{{ $post->preview }}</textarea>
      </div>
      <div class="form-group">
        <textarea type="text" style="height: 300px;" name="body" placeholder="Write your article" class="form-control">{{ $post->body }}</textarea>
      </div>
       <div class="row"> 
        <div class="col-md-12">
		  <input type="submit" class="btn main-btn pull-right btn-lg btn-primary btn-block" style="color: white; font-weight: bold"></input>
        </div>
      </div>
    </form>
  </div>

@endsection