@extends('layouts.app')

@section('content')

	<h1 class="roundborder"> <center> Publish Article </center> </h1>
  <br>

  <div class="container">

    <form method="post" action="{{ action('PostsController@store') }}">
      {{csrf_field()}}
      <div class="form-group">
        <input type="text" name="title" placeholder="Enter your title" class="form-control">
      </div>
      <div class="form-group">
        <textarea type="text" style="height: 100px;" name="preview" placeholder="Preview of article" class="form-control"></textarea>
      </div>
      <div class="form-group">
        <textarea type="text" style="height: 300px;" name="body" placeholder="Write your article" class="form-control"></textarea>
      </div>
       <div class="row"> 
        <div class="col-md-12">
          <input type="submit" class="btn main-btn pull-right btn-lg btn-primary btn-block" style="color: white; font-weight: bold"></input>
        </div>
      </div>
    </form>
  </div>

@endsection