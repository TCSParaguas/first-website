 @extends('layouts.app')

@section('content') 
 
	<br>
	<div class="buttononpostsedit">
		<a href="/questions" class="btn btn-success"> Previous page </a>
	</div>
	<br>

	<small> <center> Published on {{$question->created_at}} </center> </small>
	<hr>
	<div class="borderaroundtitle">
		<h1> <center>{{$question->question_title}}</center> </h1>
	</div>

	<br>

	<div class="borderaroundsomething">
		<div class="articleprelinestatus" id="increasefontsize">
			{{$question->further_details}}
		</div>
	</div>

	<hr>

	<div>

		<form display='inline-block' method="post" action="{{action('QuestionController@destroy', $question->id) }}">
		    @method('delete')
		    @csrf
	        <div class="form-group">
				<input type="submit" value="Delete" class="btn btn-danger">
			</div>
		</form>

	</div>


@endsection