@extends('layouts.app')
 
@section('content')

	<h1 class="roundborder"> <center> Anonymous publications </center> </h1>

	<br>


	<div class="buttononquestionspage">	
      <a href="/questions/create" class="btn main-btn pull-center btn-lg btn-success btn-block" style="color: white; font-weight: bold">
      	Publish
      </a>
      <br>
	</div>


	<form>
		<center> <input style="position: center;" type="button" class="buttonsonarticlesedit" value="Press here for an interesting fact" onclick="alert('Each day, the kidneys process about 200 quarts (50 gallons) of blood to filter out about 2 quarts of waste and water');"/> </center>
	</form>

	<br>

	<table class="table table-bordered">
		<thread>
			<tr>
				<th> Post no. </th>
				<th> Title </th>
				<th> Details </th>
				<th> View post </th>
			</tr>
		</thread>
		<tbody>
			@foreach($questions as $question)
			<tr> 
				<td href="{{action('QuestionController@show', $question->id) }}" method="question"> {{ $question->id }} </td>
				<td> {{ $question->question_title }} </td>
				<td> {{ $question->preview }} </td>
				<td>

					<form action="{{ action('QuestionController@destroy', $question->id)}}" method="post">
						<a href="{{action('QuestionController@show', $question->id) }}" class="btn btn-success" style="width: 100%;"> Read More </a>
					</form>

				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

@endsection