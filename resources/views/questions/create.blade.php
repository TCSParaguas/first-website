@extends('layouts.app')

@section('content')

  <br>
  <h1 class="roundborder"> <center> Create Post </center> </h1>  
  <br>  

  <div class="row" style="color: white;">
    <div class="col-md-6 offset-md-3">
      @if($message = Session::get('danger'))
        <div class="alert alert-danger">
          <strong>{{ $message }}</strong>
        </div>
      @endif
      <form method="post" action="{{ action('QuestionController@store') }}">
        @csrf
        <div class="form-group">
          <input class="form-control" type="text" name="question_title" placeholder="Homeostasis"/>
        </div>
        <div class="form-group">
          <textarea class="form-control" style="height: 100px" type="text" name="preview" placeholder="Homeostasis is the regulation of conditions in the body such as temperature, water content and carbon dioxide levels. Diabetes is a condition where the body cannot regulate its blood glucose levels."></textarea>
        </div>
        <div class="articleprelinestatus">
          <div class="form-group">
            <textarea class="form-control" type="text" name="further_details" placeholder=" The conditions inside our body must be very carefully controlled if the body is to function effectively. Homeostasis is the maintenance of a constant internal environment. The nervous system and hormones are responsible for this.

One example of homeostasis is the concentration of carbon dioxide in the blood being carefully controlled. Here are some of the other internal conditions that are regulated:

Body temperature
This is controlled to maintain the temperature at which the body’s enzymes work best, which is usually 37°C.

Blood sugar level
This is controlled to provide cells with a constant supply of glucose for respiration. It is controlled by the release and storage of glucose, which is in turn controlled by insulin.

Water content
This is controlled to protect cells by stopping too much water from entering or leaving them. Water content is controlled by water loss from:

the lungs - when we exhale
the skin - by sweating
the body - in urine produced by the kidneys " style="height: 200px"></textarea>
          </div>
        </div>
        <div class="row"> 
         <div class="col-md-12">
           <input type="submit" class="btn main-btn pull-right btn-lg btn-success btn-block" style="color: white; font-weight: bold"></input>
         </div>
        </div>
      </form>
    </div>
  </div>

@endsection