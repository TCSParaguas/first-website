@extends('layouts.app')

@section('content')

    <h1> <center> Profile </center> </h1>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            <ul>
                {{session('status')}}
            </ul>
        </div>
    @endif

        <hr>
        <h1 class="roundborder"> <center> Your published articles <center> </h1>


    @if(count($posts) > 0)

    <div class="container-fluid">
      <div class="row">

            @foreach($posts as $post)

                    <div class="col-sm-4 py-2">
                      <div class="card" style="background-color:#585858">
                        <div class="card-body">
                          <div class="card-body text-center">
                          <h5 class="card-title" style="color: white">{{ $post->title }}</h5>
                          <p class="card-text" style="color: white">{{ $post->preview }}</p>
                          <div class="buttonsonarticlesedit">
                            <a href="{{action('PostsController@show', $post->id) }}" class="btn btn-info" style="border-radius:4px;">Read</a> 
                          </div>
                          <br>
                          <small style="color: white;">Published on {{$post->created_at}}</small>
                          </div> 
                        </div>
                      </div>
                    </div>

            @endforeach
      </div>
    </div>

    @else

        <br>

        <h3> <center> No published articles </center> </h3>

    @endif

@endsection
