<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>{{config('app.name', 'ASCLEPIUS')}}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <style type="text/css">

            
            .homebuttonedit {
                text-align: center;
            }

            .buttonsonarticlesedit {

            }

            .buttononpostsedit {
                text-align: center;
            }

            .buttononquestionspage {
                text-align: center;
            }

            .roundborder {
                border-style: dotted;
                border-radius: 200px;
                border: 2px solid #73AD21;
                padding: 20px; 
            }

            .card {
                display: flex;
                flex-wrap: wrap;
                align-items: stretch;
                height: 550px;
                background-image: linear-gradient(to top, #459522 0%,  #87c93c 100%);
            }   

            .card {
                flex: 0 0 200px;

            }

            .quirkylittlecode {
                border-style: dotted;
                border-radius: 200px;
                border: 2px solid #0470dc;
                padding: 20px;                 
            }

            .footer {
              background-color:  #404040;
              color: white;
            }

            .showbladepostsCRUDbuttons {
              display: inline;
            }

            .articleprelinestatus {
              white-space:pre-line;
            }

            .borderaroundsomething {
              border-style: solid;
              padding-top: 5px;
              padding-right: 20px;
              padding-bottom: 20px;
              padding-left: 20px;
              border: 2px solid #459522;
            }

            .borderaroundtitle {
              border-style: solid;
              border: 2px solid #459522;

            }

        </style>

    </head>
    <body>

        <div id="app">

            @include('nvandva.navbar')
            <div class="container">
                @include('nvandva.validationalerts')
                @yield('content')
            </div>
        </div>

        <br>

        <div class='footer'>

        <footer class="page-footer font-small mdb-color pt-4">

        <div class="container text-center text-md-left">

          <div class="row text-center text-md-left mt-3 pb-3">

            <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
              <h6 class="text-uppercase mb-4 font-weight-bold">Asclepius</h6>
              <p>The purpose for this website is to help give scholars a way to post their thoughts, theories and findings in plain english. We encourage all users to make their posts as readable and as easy to 'digest' as possible. </p>
            </div>

            <hr class="w-100 clearfix d-md-none">

            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
              <h6 class="text-uppercase mb-4 font-weight-bold">Categories</h6>
              <p>
                <a href="#">Biochemistry</a>
              </p>
              <p>
                <a href="#">Cardiovascular Physiology</a>
              </p>
              <p>
                <a href="#">Immunology</a>
              </p>
              <p>
                <a href="#">Pharmacology</a>
              </p>
            </div>

            <hr class="w-100 clearfix d-md-none">

            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
              <h6 class="text-uppercase mb-4 font-weight-bold">Useful links</h6>
              <p>
                <a href="/home">Your Account</a>
              </p>
              <p>
                <a href="#">Upload diagrams</a>
              </p>
              <p>
                <a href="#">Contact us directly</a>
              </p>
              <p>
                <a href="#">Help</a>
              </p>
            </div>

            <hr class="w-100 clearfix d-md-none">

            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
              <h6 class="text-uppercase mb-4 font-weight-bold">Contact Details</h6>
              <p>
                <i class="fas fa-home mr-3"></i> Wales, UK</p>
              <p>
                <i class="fas fa-phone mr-3"></i> +44 78642 67120</p>
              <p>
                <i class="fas fa-print mr-3"></i> +44 15864 23302</p>
            </div>

          </div>

          <hr>

          <div class="row d-flex align-items-center">
            <div class="col-md-7 col-lg-8">
              <p class="text-center text-md-left">© 2019 Ascright:
                <a>
                  <strong> Asclepius.com </strong>
                </a>
              </p>

            </div>

            <div class="col-md-5 col-lg-4 ml-lg-0">
              <div class="text-center text-md-right">
                <ul class="list-unstyled list-inline">
                  <li class="list-inline-item">
                    <a class="btn-floating btn-sm rgba-white-slight mx-1">
                      <i class="fab fa-facebook-f"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a class="btn-floating btn-sm rgba-white-slight mx-1">
                      <i class="fab fa-twitter"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a class="btn-floating btn-sm rgba-white-slight mx-1">
                      <i class="fab fa-google-plus-g"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a class="btn-floating btn-sm rgba-white-slight mx-1">
                      <i class="fab fa-linkedin-in"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        </footer>

        </div>  

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>